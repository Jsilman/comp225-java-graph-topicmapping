package graphs.student;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeMap;
import java.util.Vector;
import java.util.TreeSet;

import org.apache.commons.math3.distribution.PoissonDistribution;

public class UnipartiteGraph {

	/*
	 * START: graph representation
	 */
	private TreeMap<String, Vertex> adjacencyList;
	private TreeMap<String, Vertex> bpgOrig;
	private int wordSet = 0;
	private int documentSet = 1;
	private int numEdges;
	private boolean isNullGraph;
	/*
	 * END: graph representation
	 */
	
	/*
	 * START: required functions
	 */

	public UnipartiteGraph() {
		adjacencyList = new TreeMap<String, Vertex>();
	}

	public void setGraph(BipartiteGraph bpg) {
		// PRE: -
		// POST: instantiates the unipartite graph based on a bipartite graph of words and docs;
		//       creates vertices and edges, with edge weights the word similarity z_ab
		
		TreeMap<String, Vertex> bpgAdjacencyList = bpg.adjacencyList;

		// Construct initial vertex for every word
		for(Map.Entry<String,Vertex> entry : bpgAdjacencyList.entrySet()) {
			if (entry.getValue().getSet() == wordSet){
				Vertex newVertex = new Vertex(entry.getKey());
				adjacencyList.put(entry.getKey(), newVertex);
			}
		}
		
		// For every word vertex in the BPG, get all the documents that word appears in
		// then all of those document
		for(Map.Entry<String,Vertex> entry1 : bpgAdjacencyList.entrySet()) {
			if (entry1.getValue().getSet() == wordSet){
				String word1 = entry1.getKey();
				VertexIDList adjacentDocs = entry1.getValue().getAdjs();
				Iterator<String> docsIter = adjacentDocs.iterator();
				while (docsIter.hasNext()){
					String document = docsIter.next();
					for(Map.Entry<String,Vertex> entry2 : bpgAdjacencyList.entrySet()) {
						if (entry2.getValue().getSet() == wordSet && entry2.getKey() != word1 && entry2.getValue().getAdjs().containsKey(document)){
							String word2 = entry2.getKey();
							int word1_count = bpg.numTimesWordOccursInDoc(word1, document);
							int word2_count = bpg.numTimesWordOccursInDoc(word2, document);
							double curEdgeWeight = adjacencyList.get(word1).getAdjValue(word2);
							double newEdgeWeight = word1_count * word2_count;
							adjacencyList.get(word1).addAdjWithVal(word2, curEdgeWeight + newEdgeWeight);
						}
					}
				}
			}
		}
	}

	public Double getEdgeWeight(String a, String b) {
		// PRE: a, b correspond to vertices in the graph
		// POST: returns the weight of the edge between vertices corresponding to words a and b
		VertexIDList v = adjacencyList.get(a).getAdjs();
		if (v.containsKey(b)){
			return v.get(b);
		}
		return 0.0;
	}

	public void setEdgeWeight(String a, String b, Double val) {
		// PRE: a, b correspond to vertices in the graph
		// POST: sets the weight of the edge between vertices corresponding to words a and b to be val
		adjacencyList.get(a).getAdjs().put(b, val);
		adjacencyList.get(b).getAdjs().put(a, val);
	}
	
	public void setGraphMinusStopwords(BipartiteGraph bpg, String stopFileName) {
		// PRE: stopFileName is the name of a file containing stopwords
		// POST: as the regular setGraph(), but stopwords do not become vertices
		try {
			Vector<String> stopWords = (readStopWordList(stopFileName));
			for (String word : stopWords){
				bpg.deleteVertex(word, "word");
			}
			setGraph(bpg);
		}
		catch (IOException e) {
			System.out.println("in exception: " + e);
			return;
		}
	}

	public void setNullGraph(BipartiteGraph bpg) {
		// PRE: -
		// POST: as the regular setGraph(),
		//         but sets the edge weights to be the values under the null model
		//         <z_ab> = s_a * s_b * L_C^2 + \sum_d L_d^2
		
		TreeMap<String, Vertex> bpgAdjacencyList = bpg.adjacencyList;
		
		// Construct initial vertex for every word
		for(Map.Entry<String,Vertex> entry : bpgAdjacencyList.entrySet()) {
			if (entry.getValue().getSet() == wordSet){
				Vertex newVertex = new Vertex(entry.getKey());
				adjacencyList.put(entry.getKey(), newVertex);
			}
		}
		
		//Number of words in documents added together (individual squared)
		double sumOfWordsInDocuments_Squared = 0;
		for(Map.Entry<String,Vertex> entry1 : bpgAdjacencyList.entrySet()) {
			if (entry1.getValue().getSet() == documentSet){
				int NumberOfWordsInDocument = bpg.numWordTokensInDoc(entry1.getKey());
				sumOfWordsInDocuments_Squared += (NumberOfWordsInDocument * NumberOfWordsInDocument);
			}
		}
		//The corupus squared
		double TotalNumberOfWords_Squared = bpg.numWordTokens() * bpg.numWordTokens();
		
		for(Map.Entry<String,Vertex> entry1 : bpgAdjacencyList.entrySet()) {
			if (entry1.getValue().getSet() == wordSet){
				String word1 = entry1.getKey();
				for(Map.Entry<String,Vertex> entry2 : bpgAdjacencyList.entrySet()) {
					String word2 = entry2.getKey();
					if (!word2.equals(word1) && entry2.getValue().getSet() == wordSet){
						//s_a
						double word1_occurences = bpg.numTimesWordOccurs(word1);
						//s_b
						double word2_occurences = bpg.numTimesWordOccurs(word2);
						// (S_A * S_B) / (LC^2) * \SUM_D L_D^2
						double value = ((word1_occurences * word2_occurences) / (TotalNumberOfWords_Squared)) * sumOfWordsInDocuments_Squared;
						System.out.printf("[%s,%s]((%.0f*%.0f)/%.0f)*%.0f=%.4f\n",word1,word2,word1_occurences,word2_occurences,TotalNumberOfWords_Squared,sumOfWordsInDocuments_Squared,value);
						adjacencyList.get(word1).addAdjWithVal(word2, value);
					}
				}
			}
		}

		
	}

	public void filterNoise(UnipartiteGraph nullGraph, Double p) {
		// PRE: 0 <= p <= 1
		// POST: reduce values on edges taking into account the null model graph;
		//         calculates Z_p(s_a, s_b) as per specs: the value which represents the 
		//         (1-p)-quantile of the Poisson distribution Pois_<z_ab>(z)
		
		TreeMap<String, Vertex> nullAdjacencyList = nullGraph.getAdjacencyList();
		TreeMap<String, Vertex> normAdjacencyList = adjacencyList;
		
		//For each node in the nullGraph
		for(Map.Entry<String,Vertex> node : nullAdjacencyList.entrySet()) {
			String word_a = node.getValue().getID();
			VertexIDList adjacentWords = node.getValue().getAdjs();
			Iterator<String> wordsIter = adjacentWords.iterator();
			while (wordsIter.hasNext()){
				String word_b = wordsIter.next();
				//System.out.printf("[%s,%s]\n",word_a,word_b);
				
				//The null graph contains all the lambda
				double lambda = nullAdjacencyList.get(word_a).getAdjValue(word_b);
				//The normal graph contains all the actual values (from the bipartite graph)
				double initialValue = normAdjacencyList.get(word_a).getAdjValue(word_b);
				//Calculate the inversePoisson value
				PoissonDistribution poisson = new PoissonDistribution(lambda);
				double inversePoisson = poisson.inverseCumulativeProbability(1-p);
				//System.out.printf("	[Lambda]=%.4f\n",lambda);
				//System.out.printf("	[z_%.2f]=%.4f\n",p,inversePoisson);
				//System.out.printf("	[Initial]=%.4f\n",initialValue);
				//System.out.printf("	[INT-PS]=%.4f\n",initialValue-inversePoisson);
				//System.out.printf("	[LAM-PS]=%.4f\n",lambda-inversePoisson);
				//adjacencyList.get(word_a).addAdjWithVal(word_b, (inversePoisson));
				if (inversePoisson == 0.0){
					inversePoisson = 1.0;
				}
				adjacencyList.get(word_a).addAdjWithVal(word_b, (initialValue-inversePoisson));
				//nullAdjacencyList.get(word_a).addAdjWithVal(word_b, (initialValue-inversePoisson));
			}
		}
		
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			String word1 = entry.getKey();
			VertexIDList adjacentWords = entry.getValue().getAdjs();
			Iterator<String> wordsIter = adjacentWords.iterator();
			Vector<String> toBeDeleted = new Vector<String>();
			while (wordsIter.hasNext()){
				String word2 = wordsIter.next();
				if (adjacencyList.get(word1).getAdjValue(word2) <= 0){
					toBeDeleted.add(word2);
				}
			}
			for (String s: toBeDeleted){
				adjacentWords.remove(s);
			}
		}
		
	}

	public TreeMap<String, Vertex> getAdjacencyList(){
		 return adjacencyList;
	}
	
	public Integer numVertices() {
		// PRE: -
		// POST: returns number of vertices in the graph
		return adjacencyList.size();
	}

	public Integer numEdges() {
		// PRE: -
		// POST: returns number of edges in the graph
		int totalDegreeCount = 0;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			totalDegreeCount += entry.getValue().adjs.size();
		}
		return (totalDegreeCount / 2);
	}

	public Integer degreeWord(String w) {
		// PRE: w corresponds to a vertex in the graph
		// POST: returns the degree of the vertex corresponding to word w
		return adjacencyList.get(w).adjs.size();
	}
	
	public Integer numVerticesDegreeK(Integer k) {
		// PRE: k >= 0
		// POST: returns the number of vertices that have degree k
		int vertexCount = 0;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			if (entry.getValue().adjs.size() == k)
				vertexCount++;
		}
		return vertexCount;
	}
	
	public Integer numVerticesConnectedAll() {
		// PRE: -
		// POST: returns the number of vertices that are connected to all other vertices
		int vertexCount = 0;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//If every vertex is adjacent (except for itself) then they are all connected
			if (entry.getValue().adjs.size() == numVertices() - 1)
				vertexCount++;
		}
		return vertexCount;
	}

	public Double avgDegreeOfGraph() {
		// PRE: -
		// POST: returns the average degree of all vertices in the graph;
		//       returns zero for the null graph
		double totalDegreeCount = 0;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			totalDegreeCount += (entry.getValue().adjs.size());
		}
		return totalDegreeCount / numVertices();
	}
	
	public Double propnPossibleEdges() {
		// PRE: -
		// POST: returns the proportion of actual edges to maximum possible edges;
		//       returns zero for a null graph or single-vertex graph
		double MaximumPossibleEdges = numVertices() * (numVertices() - 1);
		return (numEdges() * 2)/MaximumPossibleEdges;
		// TODO
	}
	
	/*
	 * END: required functions
	 */
	
	/*
	 * START: suggested functions
	 */
	/*
	
	public void addVertex(String s) {
		// Adds a new vertex s to the graph
	}
	*/
	
	public void deleteVertex(String s) {
		// Deletes a vertex n from the graph
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			VertexIDList v = entry.getValue().getAdjs();
			if (v.containsKey(s)){
				v.remove(s);
			}
		}
		adjacencyList.remove(s);
	}

	public void deleteEdge(String a, String b) {
		// Delete an edge between a and b if present 
	
		// TODO
	}
	

	public Vector<String> readStopWordList (String fInName) throws IOException {
		// PRE: -
		// POST: reads in stopword list, returns words as vector of strings
		
		Vector<String> stopWords = new Vector<String>();
		File stopFile = new File(fInName);

		if (stopFile.isFile()) {
				BufferedReader fIn = new BufferedReader(new FileReader(stopFile));
				String s;
				String w;
				while ((s = fIn.readLine()) != null) {
					java.util.StringTokenizer line = new java.util.StringTokenizer(s);
					while (line.hasMoreTokens()) {
						w = line.nextToken();
						stopWords.add(w);
					}
				}
				fIn.close();
		}
		return stopWords;
	}
		

	
	public void print() {
		System.out.printf("Number of nodes is %d\n", numVertices());
		System.out.printf("Number of edges is %d\n", numEdges());
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			  String vertex = entry.getKey();
			  //Prints out all vertices and there adjs (and counts of those adjs)
			  System.out.println(vertex + "("+ entry.getValue().getAdjs().size() + "): " + entry.getValue().getAdjs());
		}
	}
	
	public static void main(String[] args) {
		BipartiteGraph bg = new BipartiteGraph();
		Vector<Edge> eList;

		//String inName = "/home/madras/teaching/16comp225/ass/data/in1";
		
		String inName = "C:/Users/Jack/workspace3/TopicMappingFrame/data/in1";
		
		// replace this with your directory path
		try {
			eList = bg.readFromDirectory(inName);
			bg.setGraph(eList);
		}
		catch (IOException e) {
			System.out.println("in exception: " + e);
		}

		bg.print();

		UnipartiteGraph ug = new UnipartiteGraph();
		ug.setGraph(bg);
		System.out.println("UnipartiteGraph");
		ug.print();
		String stopWordsName = "C:/Users/Jack/workspace3/TopicMappingFrame/data/stop1.txt";

		UnipartiteGraph ug2 = new UnipartiteGraph();
		ug2.setGraphMinusStopwords(bg, stopWordsName);
		
		UnipartiteGraph ugNull = new UnipartiteGraph();
		System.out.println("NullGraph");
		ugNull.setNullGraph(bg);
		ugNull.print();
		System.out.println("edge weight btw cat, angry : " + Double.toString(ugNull.getEdgeWeight("cat","angry")));

		System.out.println("FilterNoiseGraph");
		ug.filterNoise(ugNull, 0.95);
		ug.print();
		
		PoissonDistribution poisson = new PoissonDistribution(2.1216);
		double p = 0.95;


	}

}
