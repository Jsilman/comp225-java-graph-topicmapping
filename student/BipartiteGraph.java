package graphs.student;

//import graphs.freshcode.v1.Edge;
//import graphs.freshcode.v1.Vertex;

import graphs.student.Edge;
import graphs.student.Vertex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.math.*;

public class BipartiteGraph {

	/*
	 * START: graph representation
	 */
	
	// Graph is stored in an adjacency list
	public TreeMap<String, Vertex> adjacencyList;
	private int wordSet = 0;
	private int documentSet = 1;
	private int numEdges;

	/*
	 * END: graph representation
	 */
	
	/*
	 * START: required functions
	 */

	public BipartiteGraph() {
		adjacencyList = new TreeMap<String, Vertex>();
	}

	public void setGraph(Vector<Edge> eList) {
		// PRE: -
		// POST: instantiates a bipartite graph based on a list of edges;
		//       edges represent word-document pairs
		// Instantiates a directed graph based on a list of edges
		adjacencyList.clear();

		Iterator<Edge> eIt = eList.iterator();
		while (eIt.hasNext()) { // iterate through edges
			Edge curEdge = eIt.next();
				
			String word = curEdge.getFirst();
			String document = curEdge.getSecond();
				
			// Create a new vertex for any word that does not already have one
			if (!adjacencyList.containsKey(word)) {
				Vertex v = new Vertex(word);
				//Documents are in set 0
				v.setSet(wordSet);
				adjacencyList.put(word, v);
			}
				
			// Create a new vertex for any document that does not already have one
			if (!adjacencyList.containsKey(document)) {
				Vertex v = new Vertex(document);
				//Documents are in set 1
				v.setSet(documentSet);
				adjacencyList.put(document, v);
			}
			
			// Add make the word and document adjacent (Increment is count if link already exists
			adjacencyList.get(word).addAdjAndInc(document);
			adjacencyList.get(document).addAdjAndInc(word);
		}
	}

	public Integer numVertices() {
		// PRE: -
		// POST: returns number of vertices (both word and doc) in the graph
		//Every entry in the adjacency list is a vertex therefore the size is the number of vertices;
		return adjacencyList.size();
	}
	
	public Integer numEdges() {
		// PRE: -
		// POST: returns number of edges in the graph
		int edges = 0;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			// Only need to get the edges of one set since it is a bipartite graph
			if (entry.getValue().getSet() == wordSet){ 
				edges += entry.getValue().getAdjs().size();
			}
		}
		return edges;
	}
	
	public Integer numWordTypes() {
		// PRE: -
		// POST: returns number of word types in the graph (i.e. ignoring duplicate words)
		int wordTypeCount = 0;	
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//Limit to word set
			if (entry.getValue().getSet() == wordSet){
				wordTypeCount++;
			}
		}
		return wordTypeCount;
	}
	
	public Integer numWordTokens() {
		// PRE: -
		// POST: returns number of word tokens in the graph (i.e. including duplicate words)
		int wordTokenCount = 0;	
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//Limit to word set
			if (entry.getValue().getSet() == wordSet){
				wordTokenCount += entry.getValue().getAdjs().getCount();
			}
		}
		return wordTokenCount;
	}
	
	public Integer numTimesWordOccurs(String w) {
		// PRE: -
		// POST: returns the number of times word w occurs across all documents
		int wordOccurenceCount = 0;	
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			if (entry.getKey().equals(w)){
				wordOccurenceCount += entry.getValue().getAdjs().getCount();
			}
		}
		return wordOccurenceCount;
	}
	
	public Integer numWordTypesInDoc(String d) {
		// PRE: d corresponds to a vertex in the graph
		// POST: returns number of different words (i.e. word types) that occur in document d 
		int wordTypeCount = 0;	
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//Limit to specific word set
			if (entry.getValue().getSet() == documentSet && entry.getKey().equals(d)){
				wordTypeCount = entry.getValue().getAdjs().size();
			}
		
		}
		return wordTypeCount;
	}
	
	public Integer numWordTokensInDoc(String d) {
		// PRE: d corresponds to a vertex in the graph
		// POST: returns number of total words (i.e. including duplicates) that occur in document d
		int wordTypeCount = 0;	
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//Limit to specific word set
			if (entry.getValue().getSet() == documentSet && entry.getKey().equals(d)){
				wordTypeCount = entry.getValue().getAdjs().getCount();
			}
		}
		return wordTypeCount;
	}
	
	public Integer numDocsWordOccursIn(String w) {
		// PRE: w corresponds to a vertex in the graph
		// POST: returns number of documents word w occurs in  
		int numOfDocs = 0;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//Limit to specific word set
			if (entry.getValue().getSet() == wordSet && entry.getKey().equals(w)){
				numOfDocs += entry.getValue().getAdjs().size();
			}
		}
		return numOfDocs;
	}

	public Integer numTimesWordOccursInDoc(String w, String d) {
		// PRE: d corresponds to a vertex in the graph
		// POST: returns number of times word w occurs in doc d
		int occurrences = 0;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//Find the word
			if (entry.getValue().getSet() == wordSet && entry.getKey().equals(w)){
				VertexIDList adjacents = entry.getValue().getAdjs();
				occurrences += adjacents.get(d);
			}
		}
		return occurrences;
	}
	
	public ArrayList<String> listOfWordsInSingleDocs() {
		// PRE: -
		// POST: returns the words that only occur in single documents
		//       the returned list should be ordered alphabetically and should contain no duplicates
		ArrayList<String> words = new ArrayList<String>();
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			if (entry.getValue().getSet() == wordSet){
				// If there a word only has 1 adjacent vertex then it only occurs
				// in one document
				if (entry.getValue().getAdjs().size() == 1){
					words.add(entry.getKey());
				}
			}
		}
		return words;
	}
	
	public Double propnOfWordsInSingleDocs() {
		// PRE: -
		// POST: returns the words (out of all word types) that only occur in single documents
		return (double)listOfWordsInSingleDocs().size() / numWordTypes();
	}
	
	public String mostFreqWordToken() {
		// PRE: -
		// POST: returns the most frequent word
		//       if there is more than one with maximum frequency, return the first alphabetically
		String mostFreq = null;
		int highestFreq = -1;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			//Limit to word set
			if (entry.getValue().getSet() == wordSet){
				//Count the sum of occurrences (The sum of all adjacents)
				int occurence = entry.getValue().getAdjs().getCount();
				if (occurence > highestFreq){
					mostFreq = entry.getKey();
					highestFreq = occurence;
				}
			}
		}
		return mostFreq;
	}

	public String wordInMostDocs() {
		// PRE: -
		// POST: returns the word that occurs in the largest number of documents
		//       if there is more than one with maximum frequency, return the first alphabetically
		String wordInMostDocs = "";
		int highestDocCount = -1;
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			if (entry.getValue().getSet() == wordSet){
				if (entry.getValue().adjs.size() > highestDocCount){
					wordInMostDocs = entry.getKey();
					highestDocCount = entry.getValue().adjs.size();
				}
			}
		}
		return wordInMostDocs;
	}
	
	/*
	 * END: required functions
	 */
	
	/*
	 * START: suggested functions
	 */
	/*
	public Vertex getVertex(String s, String t) {
		// Used for accessing vertex to e.g. add neighbours
		// t should be either "word" or "doc"
		return null;
	}
	
	public NavigableSet<String> getVertexSet(String t) {
		// Returns ordered set of all vertices in the graph
		// t should be either "word" or "doc"
		return null;
	}
	
	public void addVertex(String s, String t) {
		// Adds a new vertex s to the graph
		// t should be either "word" or "doc"
	}
	*/
	public void deleteVertex(String s, String t) {
		// Deletes a vertex n from the graph
		// t should be either "word" or "doc"
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			VertexIDList v = entry.getValue().getAdjs();
			if (v.containsKey(s)){
				v.remove(s);
			}
		}
		adjacencyList.remove(s);
	}
	
	public void print() {
		for(Map.Entry<String,Vertex> entry : adjacencyList.entrySet()) {
			  String vertex = entry.getKey();
			  //Prints out all vertices and there adjs (and counts of those adjs)
			  System.out.println(vertex + " => " + entry.getValue().getAdjs());
		}
	}
	/*
	public String getFirstVertexID(String t) {
		// Returns first vertex ID in TreeMap ordering
		// e.g. for starting a traversal
		// t should be either "word" or "doc"
		return null;
	}
	
	public boolean containsVertex(String s, String t) {
		// Checks if s is a vertex in the graph
		// t should be either "word" or "doc"
		return false;
	}
	*/

	/*
	 * END: suggested functions
	 */
	
	/*
	 * START: given functions
	 */
		
	public void setDefault () {
		// the sample set of edges (word-document pairs) from the specs
		
		Vector<Edge> eList = new Vector<Edge>();
		
		eList.add(new Edge("the","d1"));
		eList.add(new Edge("purple","d1"));
		eList.add(new Edge("and","d1"));		
		eList.add(new Edge("green","d1"));
		eList.add(new Edge("cat","d1"));
		eList.add(new Edge("was","d1"));
		eList.add(new Edge("angry","d1"));
		eList.add(new Edge("cat","d1"));
		eList.add(new Edge("i","d2"));
		eList.add(new Edge("heard","d2"));
		eList.add(new Edge("that","d2"));
		eList.add(new Edge("purple","d2"));
		eList.add(new Edge("and","d2"));		
		eList.add(new Edge("green","d2"));
		eList.add(new Edge("cat","d2"));
		eList.add(new Edge("is","d2"));
		eList.add(new Edge("angry","d2"));
		eList.add(new Edge("and","d2"));		
		eList.add(new Edge("green","d2"));
		eList.add(new Edge("all","d3"));
		eList.add(new Edge("cat","d3"));
		eList.add(new Edge("are","d3"));
		eList.add(new Edge("my","d3"));
		eList.add(new Edge("best","d3"));
		eList.add(new Edge("friend","d3"));
		
		this.setGraph(eList);
	}

	public Vector<Edge> readFromDirectory (String dirInName) throws IOException {
		// reads all documents from directory dirInName
		// returns vector of word-document pairs (e.g. the-d1)
		Vector<Edge> eList = new Vector<Edge>();
		File folder = new File(dirInName);

		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isFile()) {
				BufferedReader fIn = new BufferedReader(new FileReader(fileEntry));
				String s;
				String w;

				String docFileName = fileEntry.getName(); 
				String docName = docFileName.substring(0, docFileName.lastIndexOf('.'));
						
				while ((s = fIn.readLine()) != null) {
					java.util.StringTokenizer line = new java.util.StringTokenizer(s);
					while (line.hasMoreTokens()) {
						w = line.nextToken();
						eList.add(new Edge(w, docName));
					}
				}
				fIn.close();
			}
		}
		return eList;
	}

	/*
	 * END: given functions
	 */
	
	public static void main(String[] args) {
		BipartiteGraph g = new BipartiteGraph();
		Vector<Edge> eList;

		String inName = "/home/madras/teaching/16comp225/ass/data/in1";
		inName = "H:/workspace/TopicMappingFrame/data/in1";
		 inName = "C:/Users/Jack/workspace3/TopicMappingFrame/data/in1";
		// replace this with your directory path
		
		try {
			eList = g.readFromDirectory(inName);
			g.setGraph(eList);
		}
		catch (IOException e) {
			System.out.println("in exception: " + e);
		}
		g.print();
		
	}
}
